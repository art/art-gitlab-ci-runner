# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>

FROM cern/cc7-base:latest

ARG eos

COPY ${eos} /etc/yum.repos.d/

#
# Tools, Kerberos, Openssh
#
RUN yum -y install \
  cern-get-sso-cookie \
  gcc \
  git \
  krb5-workstation \
  krb5-libs \
  libffi-devel \
  openssh-clients \
  openssl-devel \
  eos-client \
  xrootd-client \
  make \
  wget \
  which \
  libXpm \
  libaio \
  zlib-devel \
  python-devel \
  python-pip \
  man \
  nano \
  redhat-lsb \
  file \
  gfal2 \
  gfal2-python \
  ShellCheck \
  HEP_OSlibs \
  && yum clean all

#
# python
#
RUN pip install --upgrade pip==20.3.4
# coverage 5.6b1, pip 20.3.4 and PyYaml 5.4.1 are the latest compatible with python 2.7
# No coverage for python2 needed as we run all test in python3
RUN pip install PyYaml==5.4.1 psutil wheel flake8 requests docopt pexpect scandir futures future argparse

#
# for vomses
#
COPY vomses.tar.gz grid-security.tar.gz ./
RUN tar zxf vomses.tar.gz -C / && tar zxf grid-security.tar.gz -C /

#
# bash shell
#
CMD /bin/bash
