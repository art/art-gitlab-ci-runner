# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>

FROM cern/cc7-base:latest

ARG eos

COPY ${eos} /etc/yum.repos.d/

#
# Tools, Kerberos, Openssh
#
RUN yum -y install \
  cern-get-sso-cookie \
  gcc \
  git \
  krb5-workstation \
  krb5-libs \
  krb5-auth-dialog \
  libffi-devel \
  openssh-clients \
  openssl-devel \
  eos-client \
  make \
  tar \
  wget \
  which \
  libXpm \
  libaio \
  zlib \
  zlib-devel \
  python-devel \
  python-pip \
  man \
  nano \
  redhat-lsb \
  file \
  pcre \
  gfal2 \
  gfal2-python \
  ShellCheck \
  HEP_OSlibs \
  && yum clean all

#
# python
#
# coverage same version as used in asetup slc6
RUN if [ "$eos" = "eos7.repo" ] ; then pip install --upgrade pip && pip install wheel && pip install flake8; fi
RUN pip install requests PyYaml docopt pexpect scandir psutil futures future argparse

#
# for vomses
#
COPY vomses.tar.gz grid-security.tar.gz ./
RUN tar zxf vomses.tar.gz -C / && tar zxf grid-security.tar.gz -C /

#
# bash shell
#
CMD /bin/bash
