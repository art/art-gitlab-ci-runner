<!-- Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration -->
Definition of the base runner images for ART CI.
These images are used for Runners on GitLab-CI.

The following images are available:

* cc7: plain cc7 with extras for running art
* cc7-artprod: plain cc7 with extras for running art, user artprod
* cc7-py3: cc7 with python3
* cc7-py3-artprod: cc7 with python3, user artprod
* grid-cc7-py3: grid setup to submit jobs to grid with python3
* grid-cc7-artprod: grid setup to submit jobs to grid, user artprod
* grid-cc7-py3-artprod: grid setup to submit jobs to grid with python3, user artprod
* grid-cc7-artprod: grid setup to submit jobs to grid, user artprod

* ruby: plain image with jekyll installed to generate website

* cs8: plain cs8 setup (still without shellcheck, yum-priorities)
* cs8-artprod: plain cs8 setup, user artprod
* grid-cs8: grid setup to submit jobs to grid
* grid-cs8-artprod: grid setup to submit jobs to grid, user artprod
